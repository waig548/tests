package waig548.Paint_v2;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Paint_v2 extends JFrame implements ActionListener
{
	TestPane tp;
	
	public Paint_v2()
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				}
				catch (IllegalAccessException| InstantiationException| UnsupportedLookAndFeelException
						| ClassNotFoundException e)
				{
					e.printStackTrace();
				}
				
				Paint_v2.this.setTitle("Paint_v2");
				Paint_v2.this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Paint_v2.this.tp= new TestPane();
				Paint_v2.this.add(Paint_v2.this.tp);
				Paint_v2.this.pack();
				Paint_v2.this.setLocationRelativeTo(null);
				Paint_v2.this.setVisible(true);
			}
		});
	}
	
	public static void main(String[] args)
	{
		new Paint_v2();
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource()== this.tp.mp.mb.itemNewFile) this.tp.cp.reset();
	}
	
	class TestPane extends JPanel
	{
		public MenuPane mp;
		public CanvasPane cp;
		
		public TestPane()
		{
			this.mp= new MenuPane();
			this.mp.setLocation(0, 0);
			this.mp.setSize(this.mp.getPreferredSize());
			this.add(this.mp);
			this.cp= new CanvasPane();
			this.cp.setLocation(0, 20);
			this.cp.setSize(this.cp.getPreferredSize());
			this.add(this.cp);
			this.setLayout(null);
		}
		
		@Override
		public Dimension getPreferredSize()
		{
			return new Dimension(800, 810);
		}
		
		class MenuPane extends JPanel
		{
			MenuBar mb;
			
			public MenuPane()
			{
				this.mb= new MenuBar();
				this.mb.setLocation(0, 0);
				this.mb.setSize(this.mb.getPreferredSize());
				this.add(this.mb);
				
				this.setLayout(null);
			}
			
			@Override
			public Dimension getPreferredSize()
			{
				return new Dimension(800, 20);
			}
			
			public class MenuBar extends JMenuBar
			{
				JMenu menuFile, menuEdit;
				JMenuItem itemNewFile;

				public MenuBar()
				{
					this.menuFile= new JMenu("File(F)");
					this.menuFile.setMnemonic(KeyEvent.VK_F);

					this.add(this.menuFile);

					this.itemNewFile= new JMenuItem("New file");
					this.itemNewFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
					this.itemNewFile.addActionListener(Paint_v2.this);

					this.menuFile.add(this.itemNewFile);
				}

				@Override
				public Dimension getPreferredSize()
				{
					return new Dimension(800, 20);
				}
			}
		}
		
		class CanvasPane extends JPanel
		{
			
			private List<List<Point>> points;
			
			public CanvasPane()
			{
				this.reset();
				MouseAdapter ma= new MouseAdapter()
				{
					private List<Point> currentPath;
					
					@Override
					public void mousePressed(MouseEvent e)
					{
						this.currentPath= new ArrayList<>(25);
						this.currentPath.add(e.getPoint());
						
						CanvasPane.this.points.add(this.currentPath);
					}
					
					@Override
					public void mouseDragged(MouseEvent e)
					{
						try
						{
							Point dragPoint= e.getPoint();
							this.currentPath.add(dragPoint);
							CanvasPane.this.repaint();
						}
						catch (NullPointerException ex)
						{
							
						}
					}
					
					@Override
					public void mouseReleased(MouseEvent e)
					{
						this.currentPath= null;
					}
				};
				
				this.addMouseListener(ma);
				this.addMouseMotionListener(ma);
				
			}
			
			@Override
			public Dimension getPreferredSize()
			{
				return new Dimension(800, 800);
			}
			
			@Override
			protected void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				Graphics2D g2d= (Graphics2D) g.create();
				for (List<Point> path : this.points)
				{
					Point from= null;
					for (Point p : path)
					{
						if (from!= null) g2d.drawLine(from.x, from.y, p.x, p.y);
						from= p;
					}
				}
				g2d.dispose();
			}
			
			public void reset()
			{
				this.points= new ArrayList<>(25);
				this.repaint();
			}
		}
	}
}
