package Waig548.Factors.Parts;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;

interface IParts
{
	JFrame base = new JFrame("Factors");
	JLabel lblText01 = new JLabel("Number:");
	JSpinner spinNum = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
	JPanel panBase = new JPanel();
	JTextArea textAOutput = new JTextArea(5, 3);
}
