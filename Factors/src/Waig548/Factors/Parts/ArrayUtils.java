package Waig548.Factors.Parts;

public class ArrayUtils
{
	public static boolean isContain(byte[] array, byte key)
	{
		for (byte element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(char[] array, char key)
	{
		for (char element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(double[] array, double key)
	{
		for (double element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(float[] array, float key)
	{
		for (float element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(int[] array, int key)
	{
		for (int element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(long[] array, long key)
	{
		for (long element : array)
			if (element == key) return true;
		return false;
	}

	public static boolean isContain(String[] array, String key)
	{
		for (String element : array)
			if (element.equals(key)) return true;
		return false;
	}

}
