package Waig548.Factors.Parts;

import java.awt.BorderLayout;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Base implements IParts, ChangeListener
{
	private int input = 1;

	public Base()
	{
		base.setBounds(100, 100, 200, 200);
		base.setVisible(true);
		base.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panBase.setLayout(null);

		base.add(panBase);
		base.add(textAOutput, BorderLayout.SOUTH);

		panBase.add(lblText01);
		panBase.add(spinNum);

		lblText01.setBounds(10, 10, 60, 20);
		spinNum.setBounds(80, 10, 90, 20);

		spinNum.addChangeListener(this);

		textAOutput.setSize(200, 100);
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		this.input = (int) spinNum.getValue();
		int[] temp = CalcFactors.call(this.input);
		BubbleSort.sort(temp);
		int count = 0;
		for (int element : temp)
			if (element == 0) count++;
		int[] output = new int[temp.length - count];
		int j = 0;
		for (int i = temp.length - 1; i > -1; i--)
			if (temp[i] != 0)
			{
				output[j] = temp[i];
				j++;
			}
		BubbleSort.sort(output);
		textAOutput.setText("Factors of " + this.input + ":" + Arrays.toString(output));
	}

}
