package Waig548.Factors.Parts;

import java.util.Arrays;

class CalcFactors
{
	public static int[] call(int input)
	{
		int[] output = new int[input];
		Arrays.fill(output, 0);
		calc(input, output);
		return output;
	}

	private static void calc(int input, int[] output)
	{
		for (int i = 0; i < input; i++)
			if ((input / (i + 1.0)) % 1 == 0 && !ArrayUtils.isContain(output, i + 1))
			{
				output[i] = i + 1;
				if (!ArrayUtils.isContain(output, input / (i + 1))) output[output.length - i - 1] = input / (i + 1);
			}
	}
}
