package Waig548.EquSolve;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Waig548.EquSolve.Components.EquSolveAvg;
import Waig548.EquSolve.Components.EquSolveCbrt;
import Waig548.EquSolve.Components.EquSolveCos;
import Waig548.EquSolve.Components.EquSolveMut;
import Waig548.EquSolve.Components.EquSolveSin;
import Waig548.EquSolve.Components.EquSolveSqrt;
import Waig548.EquSolve.Components.EquSolveSum;

public class EquSolveMain
{
	public static void main(String[] args) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter an equation(Requires\"=\"):");
		String tempstr = keyin.readLine();
		if (tempstr.matches("^[0-9]+!={1}$")) EquSolveMut.mut(tempstr);
		else if (tempstr.matches("^sin{1}[0-9]+={1}$")) EquSolveSin.sin(tempstr);
		else if (tempstr.matches("^cos{1}[0-9]+={1}$")) EquSolveCos.cos(tempstr);
		else if (tempstr.matches("^sqrt{1}[0-9]+={1}$")) EquSolveSqrt.sqrt(tempstr);
		else if (tempstr.matches("^cbrt{1}[0-9]+={1}$")) EquSolveCbrt.cbrt(tempstr);
		else if (tempstr.matches("^avg$")) EquSolveAvg.avg(null);
		else if (tempstr.matches("^sum$")) EquSolveSum.sum(null);
		else System.err.println("Wrong Format!");
	}
}