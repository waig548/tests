package Waig548.EquSolve.Components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EquSolveSum
{
	public static void sum(String tempstr) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		double temp, total = 0;
		while (true)
		{
			System.out.print("Enter the score(Enter\"stop\"to show result and exit.):");
			tempstr = keyin.readLine();
			if (!tempstr.equals("stop"))
			{
				temp = Double.parseDouble(tempstr);
				total += temp;
			}
			else break;
		}
		System.out.println("Total:" + total);
	}
}
