package Waig548.EquSolve.Components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EquSolveAvg
{
	public static void avg(String tempstr) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		double temp, total = 0, avg = 0;
		int runc = 0;
		while (true)
		{
			System.out.print("Enter the score(Enter\"stop\"to show result and exit.):");
			tempstr = keyin.readLine();
			if (!tempstr.equals("stop"))
			{
				temp = Double.parseDouble(tempstr);
				total += temp;
				runc += 1;
			}
			else break;
		}
		avg = total / runc;
		System.out.println("Average:" + avg);
	}
}
