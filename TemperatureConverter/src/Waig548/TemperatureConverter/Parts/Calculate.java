package Waig548.TemperatureConverter.Parts;

class Calculate
{
	static double convertCTo(double input, int i)
	{
		switch (i)
		{
			case 1:
				return input * 9 / 5 + 32;
			case 2:
				return input + 273.15;
			case 3:
				return (input + 273.15) * 9 / 5;
			case 4:
				return input * 33 / 100;
			case 5:
				return input * 4 / 5;
			case 6:
				return input * 21 / 40 + 7.5;
			default:
				return 0.0;
		}
	}

	static double convertToC(double input, int n)
	{
		switch (n)
		{
			case 1:
				return (input - 32) * 5 / 9;
			case 2:
				return input - 273.15;
			case 3:
				return (input - 491.67) * 5 / 9;
			case 4:
				return input * 100 / 33;
			case 5:
				return input * 5 / 4;
			case 6:
				return (input - 7.5) * 40 / 21;
			default:
				return 0.0;
		}
	}
}
