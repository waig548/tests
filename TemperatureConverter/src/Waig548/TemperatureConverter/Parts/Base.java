package Waig548.TemperatureConverter.Parts;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class Base implements IComponents
{
	
	public Base()
	{
		initialize();
		Layout.layout();
	}

	private static void initialize()
	{
		converter.setBounds(100, 100, 320, 200);
		converter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		converter.setVisible(true);
		panBase.setLayout(null);
		
		converter.add(panBase);
		converter.add(textAOutput, BorderLayout.SOUTH);
		panBase.add(lblOriginalTemp);
		panBase.add(textFOriginalTemp);
		panBase.add(cmboxOriginalTempType);
		panBase.add(btnSubmit);
		btnSubmit.addActionListener(Listeners.submit);
	}
}
