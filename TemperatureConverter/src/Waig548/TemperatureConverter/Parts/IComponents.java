package Waig548.TemperatureConverter.Parts;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

interface IComponents
{
	JFrame converter = new JFrame("Temperature Converter");
	JPanel panBase = new JPanel();
	JLabel lblOriginalTemp = new JLabel("原始溫度:");
	JTextField textFOriginalTemp = new JTextField();
	String[] strTempType = { "攝氏(°C)", "華氏(°F)", "開氏(K)", "蘭金(°R)", "牛頓(°N)", "列氏(°Re)", "羅氏(°Ro)" };
	double[] temperature = new double[strTempType.length];
	JComboBox<String> cmboxOriginalTempType = new JComboBox<String>(strTempType);
	JTextArea textAOutput = new JTextArea(7, 10);
	JButton btnSubmit = new JButton("Submit");
}
