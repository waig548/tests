package Waig548.TemperatureConverter.Parts;

class Layout implements IComponents
{
	public static void layout()
	{
		lblOriginalTemp.setBounds(10, 10, 60, 20);
		textFOriginalTemp.setBounds(70, 10, 60, 20);
		cmboxOriginalTempType.setBounds(135, 10, 80, 20);
		btnSubmit.setBounds(220, 10, 80, 20);
	}
}
