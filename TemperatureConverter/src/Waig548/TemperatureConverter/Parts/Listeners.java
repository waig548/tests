package Waig548.TemperatureConverter.Parts;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

class Listeners implements IComponents
{
	private static double originalTemp;

	public static ActionListener submit = new ActionListener()
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{

			Arrays.fill(temperature, 0.0);
			if (e.getSource() == btnSubmit) originalTemp = Double.parseDouble(textFOriginalTemp.getText());
			int n = 0;
			switch (cmboxOriginalTempType.getSelectedIndex())
			{
				case 0:
					temperature[0] = originalTemp;
					n = 0;
					break;
				case 1:
					temperature[1] = originalTemp;
					n = 1;
					break;
				case 2:
					temperature[2] = originalTemp;
					n = 2;
					break;
				case 3:
					temperature[3] = originalTemp;
					n = 3;
					break;
				case 4:
					temperature[4] = originalTemp;
					n = 4;
					break;
				case 5:
					temperature[5] = originalTemp;
					n = 5;
					break;
				case 6:
					temperature[6] = originalTemp;
					n = 6;
					break;
			}
			if (temperature[0] == 0.0) temperature[0] = Calculate.convertToC(originalTemp, n);
			for (int i = 1; i < temperature.length; i++)
				temperature[i] = Calculate.convertCTo(temperature[0], i);
			output();
		}

		private void output()
		{
			textAOutput.setText("");
			for (int i = 0; i < temperature.length; i++)
				if (i == temperature.length - 1)
					textAOutput.setText(textAOutput.getText() + strTempType[i] + ":" + temperature[i]);
				else textAOutput.setText(textAOutput.getText() + strTempType[i] + ":" + temperature[i] + "\n");
		}
	};
}
