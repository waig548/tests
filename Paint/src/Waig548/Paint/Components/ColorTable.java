package Waig548.Paint.Components;

import java.awt.Color;

interface ColorTable
{
	final Color BLACK = new Color(25, 25, 25);
	final Color RED = new Color(255, 0, 0);
	final Color GREEN = new Color(0, 127, 14);
	final Color BROWN = new Color(114, 69, 40);
	final Color BLUE = new Color(0, 38, 255);
	final Color PURPLE = new Color(140, 0, 234);
	final Color CYAN = new Color(0, 159, 191);
	final Color LIGHTGRAY = new Color(151, 151, 151);
	final Color GRAY = new Color(64, 64, 64);
	final Color PINK = new Color(255, 186, 217);
	final Color LIME = new Color(0, 255, 0);
	final Color YELLOW = new Color(255, 255, 0);
	final Color LIGHTBLUE = new Color(127, 212, 255);
	final Color MAGENTA = new Color(255, 0, 220);
	final Color ORANGE = new Color(255, 106, 0);
	final Color WHITE = new Color(255, 255, 255);

}
