package Waig548.Paint.Components;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

interface IComponents extends ColorTable
{
	JButton btnTools[] = new JButton[6];
	final String[] textTools = { "line", "rect", "elli", "solid rect", "solid elli", "clear" };
	JButton btnColors[] = new JButton[16];
	final Color crs[] = { BLACK, RED, GREEN, BROWN, BLUE, PURPLE, CYAN, LIGHTGRAY, GRAY, PINK, LIME, YELLOW, LIGHTBLUE,
			MAGENTA, ORANGE, WHITE };
	CGPanel drawpan = new CGPanel();
	JPanel toolbox = new JPanel();
	JPanel pattles = new JPanel();
	final Border RAISED = BorderFactory.createBevelBorder(BevelBorder.RAISED);
	final Border LOWERED = BorderFactory.createBevelBorder(BevelBorder.LOWERED);

}
