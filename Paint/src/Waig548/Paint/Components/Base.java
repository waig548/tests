package Waig548.Paint.Components;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;

public class Base extends JFrame implements IComponents
{
	static String drawToolc;
	static Color drawColorc;
	static Point p1;
	static Point p2;
	static boolean is_draw = false;
	static int count = 0;
	static String[] prevTools=new String[100];
	static Color[] prevColors=new Color[100];
	static Point[] prevP1=new Point[100];
	static Point[] prevP2=new Point[100];

	public Base()
	{
		drawpan.setBounds(20, 20, 300, 300);
		this.add(drawpan);
		drawpan.setBorder(BorderFactory.createLineBorder(crs[0], 5));
		drawpan.addMouseListener(new CGetPoint());
		drawpan.addMouseMotionListener(new CGetPoint());

		for (int i = 0; i < btnTools.length; i++)
			btnTools[i] = new JButton(textTools[i]);
		toolbox.setLayout(null);
		toolbox.setBounds(330, 20, 130, 170);
		this.add(toolbox);

		pattles.setLayout(null);
		pattles.setBounds(330, 190, 130, 130);
		this.add(pattles);

		for (int i = 0; i < btnTools.length; i++)
		{
			btnTools[i] = new JButton(textTools[i]);
			toolbox.add(btnTools[i]);
			btnTools[i].setBounds(10, i * 30, 110, 20);
			btnTools[i].setBorder(RAISED);
			btnTools[i].addMouseListener(new CTool());
		}

		int k = 0;
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
			{
				btnColors[k] = new JButton();
				pattles.add(btnColors[k]);
				btnColors[k].setBounds(10 + j * 30, 10 + i * 30, 20, 20);
				btnColors[k].setBorder(RAISED);
				btnColors[k].setBackground(crs[k]);
				btnColors[k].addMouseListener(new CColor());
				k++;
			}

		drawToolc = btnTools[0].getText();
		btnTools[0].setBorder(LOWERED);
		drawColorc = crs[0];
		btnColors[0].setBorder(LOWERED);
		this.setTitle("Paint");
		this.setLayout(null);
		this.setBounds(50, 50, 480, 370);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	class CGetPoint extends MouseAdapter
	{
		@Override
		public void mousePressed(MouseEvent e)
		{
			p1 = e.getPoint();
		}

		@Override
		public void mouseDragged(MouseEvent e)
		{
			p2 = e.getPoint();
			is_draw = true;
			Base.this.repaint();
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
			if (is_draw) count++;
			is_draw = false;
		}
	}

	class CTool extends MouseAdapter
	{
		@Override
		public void mouseClicked(MouseEvent e)
		{
			for (int i = 0; i < btnTools.length - 1; i++)
			{
				btnTools[i].setBorder(RAISED);
				if (e.getSource() == btnTools[i])
				{
					drawToolc = btnTools[i].getText();
					btnTools[i].setBorder(LOWERED);
				}
			}
			if (e.getSource() == btnTools[5])
			{
				count = 0;
				prevTools = new String[100];
				prevColors = new Color[100];
				prevP1 = new Point[100];
				prevP2 = new Point[100];
				Base.this.repaint();
			}
		}
	}

}
