package Waig548.Paint.Components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

class CGPanel extends JPanel implements IComponents
{
	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		if (!Base.is_draw) return;

		for (int i = 0; i < Base.count; i++)
		{
			g2.setColor(Base.prevColors[i]);
			draw(g2, Base.prevTools[i], Base.prevP1[i].x, Base.prevP1[i].y, Base.prevP2[i].x, Base.prevP2[i].y);
		}

		g2.setColor(Base.drawColorc);
		draw(g2, Base.drawToolc, Base.p1.x, Base.p1.y, Base.p2.x, Base.p2.y);

		Base.prevTools[Base.count] = Base.drawToolc;
		Base.prevColors[Base.count] = Base.drawColorc;
		Base.prevP1[Base.count] = Base.p1;
		Base.prevP2[Base.count] = Base.p2;
	}

	private void draw(Graphics2D g2, String tool, int px, int py, int px2, int py2)
	{
		if (tool.equals("line")) g2.draw(new Line2D.Double(px, py, px2, py2));

		double x, y, w, h;
		x = px;
		y = py;
		w = Math.abs(px - px2);
		h = Math.abs(py - py2);
		if (px2 < px) x = px2;
		if (py2 < py) y = py2;
		if (tool.equals("rect")) g2.draw(new Rectangle2D.Double(x, y, w, h));
		if (tool.equals("elli")) g2.draw(new Ellipse2D.Double(x, y, w, h));
		if (tool.equals("solid rect")) g2.fill(new Rectangle2D.Double(x, y, w, h));
		if (tool.equals("solid elli")) g2.fill(new Ellipse2D.Double(x, y, w, h));
	}
}
