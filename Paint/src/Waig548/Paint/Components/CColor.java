package Waig548.Paint.Components;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class CColor extends MouseAdapter implements IComponents
{
	@Override
	public void mouseClicked(MouseEvent e)
	{
		for (int k = 0; k < btnColors.length; k++)
		{
			btnColors[k].setBorder(RAISED);
			if (e.getSource() == btnColors[k])
			{
				Base.drawColorc = crs[k];
				btnColors[k].setBorder(LOWERED);
			}
		}
	}
}
