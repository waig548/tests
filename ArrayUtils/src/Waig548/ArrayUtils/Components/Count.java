package Waig548.ArrayUtils.Components;

public class Count
{
	private static int calc(int[] input)
	{
		int count_max = 0, maxnum = input[0];
		for (int element : input)
		{
			int count = 0;
			for (int element2 : input)
				if (element2 == element) count++;
			if (count > count_max)
			{
				count_max = count;
				maxnum = element;
			}
		}
		return maxnum;
	}

	public static int count(String inputstr)
	{
		inputstr = inputstr.substring(4);
		String[] inputstra = inputstr.split(",");
		int[] input = ConvertArray.convert(inputstra);
		return calc(input);
	}
}
