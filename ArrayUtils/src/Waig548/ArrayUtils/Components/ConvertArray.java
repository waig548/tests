package Waig548.ArrayUtils.Components;

public class ConvertArray
{
	public static int[] convert(String[] tempa)
	{
		int input[] = new int[tempa.length];
		for (int i = 0; i < tempa.length; i++)
			input[i] = Integer.parseInt(tempa[i]);
		return input;
	}
}
