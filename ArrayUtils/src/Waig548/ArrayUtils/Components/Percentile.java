package Waig548.ArrayUtils.Components;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Percentile
{
	private static int calc(int[] input, int n)
	{
		double temp = input.length * n / 100;
		int pos = (int) Math.floor(temp);
		if (temp % 1 == 0) return (input[pos] + input[pos - 1]) / 2;
		else return input[pos];
	}

	public static void pn(String inputstr) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		inputstr = inputstr.substring(2);
		String[] inputstra = inputstr.split(",");
		int[] input = ConvertArray.convert(inputstra);
		BubbleSort.sort(input);
		System.out.print("Percentage:");
		System.out.println(calc(input, Integer.parseInt(keyin.readLine())));
	}

}
