package Waig548.ArrayUtils.Components;

public class MidNum
{

	private static int calc(int[] input)
	{
		int output;
		if (input.length % 2 == 0) output = (input[input.length / 2] + input[input.length / 2 - 1]) / 2;
		else output = input[(input.length - 1) / 2];
		return output;
	}

	public static int mid(String inputstr)
	{
		inputstr = inputstr.substring(7);
		String[] inputstra = inputstr.split(",");
		int[] input = ConvertArray.convert(inputstra);
		return calc(input);
	}
}
