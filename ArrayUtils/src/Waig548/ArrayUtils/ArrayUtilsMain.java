package Waig548.ArrayUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Waig548.ArrayUtils.Components.Count;
import Waig548.ArrayUtils.Components.MidNum;
import Waig548.ArrayUtils.Components.Percentile;
import Waig548.ArrayUtils.Components.Quartile;

public class ArrayUtilsMain
{

	public static void main(String[] args) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter an augment and an array to continue:");
		String inputstr = keyin.readLine();
		if (inputstr.matches("^midnum{1} [[0-9]+,[0-9]+]$")) System.out.println(MidNum.mid(inputstr));
		if (inputstr.matches("^count [[0-9]+,[0-9]+]$")) System.out.println(Count.count(inputstr));
		if (inputstr.matches("^P [[0-9]+,[0-9]+]")) Percentile.pn(inputstr);
		if (inputstr.matches("^Q [[0-9]+,[0-9]+]")) Quartile.qn(inputstr);
	}

}
