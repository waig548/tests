package Waig548.Sorts.Components;

import java.util.Arrays;

public class HeapSort
{
	private static int[] a;
	private static int n;
	private static int left;
	private static int right;
	private static int largest;

	private static void buildHeap(int[] a)
	{
		n = a.length - 1;
		for (int i = n / 2; i >= 0; i--)
			maxHeap(a, i);
	}

	private static void maxHeap(int[] a, int i)
	{
		left = 2 * i;
		right = 2 * i + 1;
		if (left < n && a[left] > a[i]) largest = left;
		else largest = i;
		if (right <= n && a[right] > a[largest]) largest = right;
		if (largest != i)
		{
			Exchange.exchange(a, i, largest);
			maxHeap(a, largest);
		}
	}

	private static void heapSort(int[] input)
	{
		a = input;
		buildHeap(a);
		for (int i = n; i > 0; i--)
		{
			Exchange.exchange(input, 0, i);
			n--;
			maxHeap(a, 0);
		}
	}

	public static int[] heap(String tempstr)
	{
		tempstr = tempstr.substring(5);
		String[] tempa = tempstr.split(",");
		int input[] = ConvertArray.convert(tempa);
		System.out.println("Input:" + Arrays.toString(input));
		heapSort(input);
		return input;
	}
}
