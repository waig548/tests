package Waig548.Sorts.Components;

import java.util.Arrays;

public class CountingSort
{
	private static int max;
	private static int min;

	private static void maxmin(int[] input)
	{
		max = input[0];
		min = input[0];
		for (int i = 1; i < input.length; i++)
			if (input[i] < min) min = input[i];
			else if (input[i] > max) max = input[i];
	}

	private static void sort(int[] input)
	{
		maxmin(input);
		int counts[] = new int[max - min + 1];
		for (int i = 0; i < input.length; i++)
			counts[input[i] - min]++;
		counts[0]--;
		for (int i = 1; i < counts.length; i++)
			counts[i] = counts[i] + counts[i - 1];
		for (int i = input.length - 1; i >= 0; i--)
		{
			int[] aux = new int[input.length];
			aux[counts[input[i] - min]--] = input[i];
		}
	}

	public static int[] counting(String tempstr)
	{
		tempstr = tempstr.substring(9);
		String[] tempa = tempstr.split(",");
		int input[] = ConvertArray.convert(tempa);
		System.out.println("Input: " + Arrays.toString(input));
		sort(input);
		return input;
	}
}
