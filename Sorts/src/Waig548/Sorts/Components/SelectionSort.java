package Waig548.Sorts.Components;

import java.util.Arrays;

public class SelectionSort
{

	private static void sort(int[] input)
	{
		for (int i = 0; i < input.length; i++)
			for (int j = i; j < input.length; j++)
				if (input[i] > input[j]) input = Exchange.exchange(input, i, j);
	}

	public static int[] select(String tempstr)
	{
		tempstr = tempstr.substring(7);
		String[] tempa = tempstr.split(",");
		int[] input = ConvertArray.convert(tempa);
		System.out.println("Input: " + Arrays.toString(input));
		sort(input);
		return input;
	}
}
