package Waig548.Sorts.Components;

import java.util.Arrays;

public class InsertionSort
{
	private static void sort(int[] input)
	{
		for (int i = 1; i < input.length; i++)
		{
			for (int j = i; j > 0; j--)
				if (input[j] < input[j - 1]) Exchange.exchange(input, j, j - 1);
			//System.out.println("pass " + i + ": " + Arrays.toString(input));
		}
	}

	public static int[] insert(String tempstr)
	{
		tempstr = tempstr.substring(7);
		String[] tempa = tempstr.split(",");
		int input[] = ConvertArray.convert(tempa);
		System.out.println("Input: " + Arrays.toString(input));
		sort(input);
		return input;
	}
}
