package Waig548.Sorts.Components;

import java.util.Arrays;

public class BubbleSort
{
	private static void sort(int[] input)
	{
		for (int i = (input.length - 1); i > 0; i--)
			for (int j = 0; j < input.length - 1; j++)
				if (input[j] > input[j + 1]) Exchange.exchange(input, j, j + 1);
	}

	public static int[] bubble(String tempstr)
	{
		tempstr = tempstr.substring(7);
		String[] tempa = tempstr.split(",");
		int input[] = ConvertArray.convert(tempa);
		System.out.println("Input:" + Arrays.toString(input));
		sort(input);
		return input;
	}

}
