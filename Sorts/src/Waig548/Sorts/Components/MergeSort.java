package Waig548.Sorts.Components;

import java.util.Arrays;

public class MergeSort
{
	private static void mergeSort(int[] input)
	{
		if (input.length > 1)
		{
			int[] left = leftHalf(input);
			int[] right = rightHalf(input);
			mergeSort(left);
			mergeSort(right);
			merging(input, left, right);
		}
	}

	private static void merging(int[] result, int[] left, int[] right)
	{
		int i1 = 0, i2 = 0;
		for (int i = 0; i < result.length; i++)
			if (i2 >= right.length || (i1 < left.length && left[i1] <= right[i2]))
			{
				result[i] = left[i1];
				i1++;
			}
			else
			{
				result[i] = right[i2];
				i2++;
			}
	}

	private static int[] leftHalf(int[] input)
	{
		int[] left = new int[input.length / 2];
		for (int i = 0; i < input.length / 2; i++)
			left[i] = input[i];
		return left;
	}

	private static int[] rightHalf(int[] input)
	{
		int[] right = new int[input.length - input.length / 2];
		for (int i = 0; i < input.length - input.length / 2; i++)
			right[i] = input[i + input.length / 2];
		return right;
	}

	public static int[] merge(String tempstr)
	{
		tempstr = tempstr.substring(6);
		String[] tempa = tempstr.split(",");
		int[] input = ConvertArray.convert(tempa);
		System.out.println("Input:" + Arrays.toString(input));
		mergeSort(input);
		return input;
	}
}
