package Waig548.Sorts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import Waig548.Sorts.Components.BubbleSort;
import Waig548.Sorts.Components.CountingSort;
import Waig548.Sorts.Components.HeapSort;
import Waig548.Sorts.Components.InsertionSort;
import Waig548.Sorts.Components.MergeSort;
import Waig548.Sorts.Components.SelectionSort;

public class SortsMain
{
	public static void main(String[] args) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		int[] output = null;
		System.out.print("Enter a array and specify which sort algortihm you want to use.(Use\',\' to split numbers):");
		String tempstr = keyin.readLine();
		if (tempstr.matches("^bubble{1} [[0-9],[0-9]]+$")) output = BubbleSort.bubble(tempstr);
		else if (tempstr.matches("^counting{1} [[0-9],[0-9]]+$")) output = CountingSort.counting(tempstr);
		else if (tempstr.matches("^select{1} [[0-9],[0-9]]+$")) output = SelectionSort.select(tempstr);
		else if (tempstr.matches("^merge{1} [[0-9],[0-9]]+$")) output = MergeSort.merge(tempstr);
		else if (tempstr.matches("^heap{1} [[0-9],[0-9]]+$")) output = HeapSort.heap(tempstr);
		else if (tempstr.matches("^insert{1} [[0-9],[0-9]]+$")) output = InsertionSort.insert(tempstr);
		else System.err.println("Wrong Format!");
		int min = output[0];
		int max = output[output.length - 1];
		System.out.println("Sorted: " + Arrays.toString(output));
		System.out.println();
		System.out.println("Max:" + max);
		System.out.println("Min:" + min);

	}
}
