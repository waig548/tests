package Waig548.ArrayComp.Components;

public class Exchange
{
	public static int[] exchange(int[] input, int i, int j)
	{
		int temp = input[i];
		input[i] = input[j];
		input[j] = temp;
		return input;
	}

}
