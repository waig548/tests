package Waig548.ArrayComp.Components;

public class BubbleSort
{
	public static void sort(int[] input)
	{
		for (int i = (input.length - 1); i > 0; i--)
			for (int j = 0; j <= input.length - 2; j++)
				if (input[j] > input[j + 1]) Exchange.exchange(input, j, j + 1);
	}
}
