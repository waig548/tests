package Waig548.ArrayComp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import Waig548.ArrayComp.Components.BubbleSort;
import Waig548.ArrayComp.Components.ConvertArray;

public class ArrayComp
{
	private static int[] maxmin(int[] input)
	{
		int max = input[input.length - 1], min = input[0];
		int[] output = { min, max };
		return output;
	}

	public static void main(String[] args) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		String[][] inputstr = { null, null, null };
		int[][] input = { new int[100], new int[100], new int[100] };
		int[] max = new int[3], min = new int[3];

		System.out.print("Press Enter to continue or use\"random\" to run this program with random arrays:");

		if (keyin.readLine().equals("random")) for (int i = 0; i < 3; i++)
			for (int j = 0; j < 100; j++)
				input[i][j] = (int) (Math.random() * 100 + 1);
		else if (keyin.readLine().equals(""))
		{
			for (int i = 0; i < 3; i++)
			{
				System.out.println("Enter array" + i + "(Max 100 items, use \",\" to split items.):");
				String temp = keyin.readLine();
				inputstr[i] = temp.split(",");
			}
			for (int i = 0; i < 3; i++)
				input[i] = ConvertArray.convert(inputstr[i]);
		}
		else System.exit(0);

		for (int i = 0; i < 3; i++)
			System.out.println("Input" + i + ":" + Arrays.toString(input[i]));

		System.out.println();

		for (int i = 0; i < 3; i++)
			BubbleSort.sort(input[i]);

		for (int i = 0; i < 3; i++)
		{
			int[] output = maxmin(input[i]);
			min[i] = output[0];
			max[i] = output[1];
			System.out.println();
			System.out.println("Minimum of array" + i + ":" + output[0]);
			System.out.println("Maximum of array" + i + ":" + output[1]);
		}

		BubbleSort.sort(max);
		BubbleSort.sort(min);

		System.out.println();
		System.out.println("Minimum of all:" + min[0]);
		System.out.println("Maximum of all:" + max[2]);
	}

}
