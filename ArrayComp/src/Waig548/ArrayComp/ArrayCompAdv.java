package Waig548.ArrayComp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Waig548.ArrayComp.Components.BubbleSort;

public class ArrayCompAdv
{
	public static void main(String[] agrs) throws IOException
	{
		BufferedReader keyin = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("How many classes:");
		int classnum = Integer.parseInt(keyin.readLine());
		String[][] tempstr = new String[classnum][];
		int[][] input;
		int[] num = new int[classnum];
		for (int i = 0; i < classnum; i++)
		{
			System.out.print("Scores for Class" + (i + 1) + "(Use \",\" to split data.):");
			tempstr[i] = keyin.readLine().split(",");
			num[i] = tempstr[i].length;
		}
		input = new int[classnum][];
		for (int i = 0; i < classnum; i++)
		{
			input[i] = new int[tempstr[i].length];
			for (int j = 0; j < tempstr[i].length; j++)
				input[i][j] = Integer.parseInt(tempstr[i][j]);
		}
		for (int i = 0; i < classnum; i++)
			System.out.print("\tClass" + (i + 1));
		BubbleSort.sort(num);
		System.out.println();
		for (int i = 0; i < num[num.length - 1]; i++)
		{
			System.out.print(i + 1);
			for (int[] element : input)
				if (i < element.length) System.out.print("\t" + element[i]);
				else System.out.print("\t");
			System.out.println();
		}
		for (int[] element : input)
			BubbleSort.sort(element);
		System.out.print("Min");
		for (int[] element : input)
			System.out.print("\t" + element[0]);
		System.out.print("\nMax");
		for (int[] element : input)
			System.out.print("\t" + element[element.length - 1]);
	}
}
